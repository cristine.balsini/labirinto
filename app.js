const map1 = [
  "WWWWWWWWWWWWWWWWWWWWW",
  "W   W     W     W W W",
  "W W W WWW WWWWW W W W",
  "W W W   W     W W   W",
  "W WWWWWWW W WWW W W W",
  "W         W     W W W",
  "W WWW WWWWW WWWWW W W",
  "W W   W   W W     W W",
  "W WWWWW W W W WWW W F",
  "S     W W W W W W WWW",
  "WWWWW W W W W W W W W",
  "W     W W W   W W W W",
  "W WWWWWWW WWWWW W W W",
  "W       W       W   W",
  "WWWWWWWWWWWWWWWWWWWWW",
];

//////////////// criar array bidimensional [colunas][linhas] ///////////////////////
for (const i in map1) {
  map1[i] = map1[i].split('');
}
//////////////// atribui classe para os elementos ///////////////////////////////////
for (const colunas in map1) {
  const coluna = document.createElement('div');
  coluna.className = 'colunas';
  for (const linhas in map1[colunas]) {
    const linha = document.createElement('div');
    if (map1[colunas][linhas] === 'W') {
      linha.className = 'paredes';
    }
    else if (map1[colunas][linhas] === ' ') {
      linha.className = 'caminho'
    }
    else if (map1[colunas][linhas] === 'S') {
      const saida = document.createElement('div');
      saida.id = 'jogador';
      linha.appendChild(saida);
      linha.className = 'saida';
    }
    else if (map1[colunas][linhas]) {
      linha.className = 'final';
    }
    coluna.appendChild(linha);
  }
  labirinto.appendChild(coluna);
}


/////////////////////////////// inicio do evento ///////////////////////////////////
document.addEventListener('keydown', (event) => {
  keyboard = event.key;

  const colunas = document.getElementsByClassName('colunas');
  const jogador = document.getElementById('jogador');


  for (let i = 0; i < colunas.length; i++) {

    for (let linhas = 0; linhas < colunas[i].children.length; linhas++) {

      if (colunas[i].children[linhas].firstChild !== null) {

        if (keyboard === 'ArrowDown') {
          if (condicional(colunas[i + 1].children[linhas])) {
            colunas[i + 1].children[linhas].appendChild(jogador);
          }
          return;
        }

        else if (keyboard === 'ArrowUp') {
          if (condicional(colunas[i - 1].children[linhas])) {
            colunas[i - 1].children[linhas].appendChild(jogador);
          }
          return;
        }

        else if (keyboard === 'ArrowLeft') {
          if (condicional(colunas[i].children[linhas - 1])) {
            colunas[i].children[linhas - 1].appendChild(jogador);
          }
          return;
        }

        else if (keyboard === 'ArrowRight') {
          if (condicional(colunas[i].children[linhas + 1])) {
            colunas[i].children[linhas + 1].appendChild(jogador);
            if (vitoria(colunas[i].children[linhas + 1])) {
              let x = document.createElement('p');
              x.className = 'paragrafo';
              x.innerText = 'Você venceu!';
              document.body.appendChild(x);
              colunas[i].children[linhas + 1].removeChild(jogador);
            }
          }
          return;
        }

      }
    }
  }
});

function condicional(elemento) {
  if (verificaElemento(elemento)) {
    //se for classe paredes, nao aceita movimentação
    if (elemento.className === 'paredes') {
      return false;
    }
    else { return true; }
  }
}

function verificaElemento(x) {
  //limpa erro do console para quando houver tentativa de append fora do labirinto
  if (x === undefined) {
    return false;
  }
  else {
    return true;
  }
}

function vitoria(elemento) {
  //se seta for pra cima e for classe paredes, nao aceita (na condição do append para classe 'final')
  if (elemento.className === 'final') {
    return true;
  }
  else { return false }
}

