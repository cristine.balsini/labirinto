
const map = [
  "WWWWWWWWWWWWWWWWWWWWW",
  "W   W     W     W W W",
  "W W W WWW WWWWW W W W",
  "W W W   W     W W   W",
  "W WWWWWWW W WWW W W W",
  "W         W     W W W",
  "W WWW WWWWW WWWWW W W",
  "W W   W   W W     W W",
  "W WWWWW W W W WWW W F",
  "S     W W W W W W WWW",
  "WWWWW W W W W W W W W",
  "W     W W W   W W W W",
  "W WWWWWWW WWWWW W W W",
  "W       W       W   W",
  "WWWWWWWWWWWWWWWWWWWWW",
];

let mapSplit = [];  //array com elementos separados por letras e espaços em branco
for (let i = 0; i < map.length; i++) {
  mapSplit.push(map[i].split(''));
}


let linhas = map.length;  //15 linhas
let colunas = map[0].length;  //21 colunas
let labirinto = [];   //array que armazena todos elementos como um array unidimensional
for (let j = 0; j < linhas; j++) {
  for (let i = 0; i < colunas; i++) {
    let celulas = mapSplit[j][i];
    labirinto.push(celulas);
  }
}

//LOOPING POR TODAS AS LINHAS E TODAS AS COLUNAS E CRIAR DIVS PARA CADA ELEMENTO
function myElements() {
  for (let i = 0; i < 15; i++) {
    for (let j = 0; j < 21; j++) {
      let x = document.createElement('div');
      x.textContent = myMap[i][j];
      x.className = 'padrao';
      labirinto.appendChild(x);
    }
  }
}
myElements();


//VARIAVEL SELETORES ARMAZENA TODOS ELEMENTOS DA CLASSE '.PADRAO'. O LOOP DEFINIU AS CLASSES NO CSS
let seletores = document.querySelectorAll('.padrao');
for (let i = 0; i < seletores.length; i++) {
  if (seletores[i].innerText === 'W') {
    seletores[i].style.backgroundColor = 'rgb(40, 134, 134)';
    seletores[i].className = 'paredes';
  }
  else if (seletores[i].innerText === 'S') {
    seletores[i].style.backgroundColor = 'rgb(236, 240, 15)';
    seletores[i].className = 'jogador';
    seletores[i].id = 'jogador';
  }
  else if (seletores[i].innerText === 'F') {
    seletores[i].style.backgroundColor = 'red';
    seletores[i].className = 'final';
  }
  else {
    seletores[i].className = 'caminho';
  }
}

let saida = document.createElement('div');
saida.className = 'saida';
jogador.appendChild(saida);



///////////////////////////////////// INICIO DO EVENTO /////////////////////////////////////////////
const jogador = document.getElementById('jogador');
const final = document.getElementById('final');


////// função que desabilita o keydown //////////
function desabilita(event) {
  event.preventDefault;
}

/// preciso mudar a abordagem das funções pois desse jeito serão muitas condições e não vai dar certo ...
function moveDireita() {
  document.addEventListener('keydown', (eventDireita) => {
    const keyboard = eventDireita.key;
    if (keyboard === 'ArrowRight') {

      let direita = parseInt(jogador.style.left.replace('px', ''));

      if (jogador.style.left === '210px') {
        desabilita();
      }

      if (isNaN(direita)) {
        direita = 0;
      }
      direita += 42;
      jogador.style.left = direita + 'px';
    }

  });

}
moveDireita();
moverEsquerda();
moverCima()
moverBaixo();


function moverEsquerda() {
  document.addEventListener('keydown', (eventEsquerda) => {
    keydown = eventEsquerda.key;

    if (keydown === 'ArrowLeft') {

      let esquerda = parseInt(jogador.style.left.replace('px', ''));
      if (isNaN(esquerda)) {
        esquerda = 0;
      }
      esquerda -= 42;
      jogador.style.left = esquerda + 'px';
    }
  });
}

function moverCima() {
  document.addEventListener('keydown', (eventCima) => {
    keydown = eventCima.key;

    if (keydown === 'ArrowUp') {
      let cima = parseInt(jogador.style.top.replace('px', ''));

      if (jogador.style.left === '42px') {

        if (isNaN(cima)) {
          cima = 0;
        }
        console.log(cima)
        cima -= 42;
        jogador.style.top = cima + 'px';
      }
    }
  });
}


function moverBaixo() {
  document.addEventListener('keydown', (eventBaixo) => {
    keydown = eventBaixo.key;

    if (keydown === 'ArrowDown') {
      let baixo = parseInt(jogador.style.top.replace('px', ''));
      if (isNaN(baixo)) {
        baixo = 0;
      }
      baixo += 42;
      jogador.style.top = baixo + 'px';
    }
  });
}
